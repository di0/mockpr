﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPr_INTERVIEW
{
    class Program_INTERVIEW
    {
        private const string NEW_LICENSE_TEXT =
            "****** COPYRIGHT 2019 OSISOFT ******";

        static void Main(string[] args)
        {
            var unitTests = new MyUtilityTests();
            unitTests.RunAll();

            string rootFolder = (string)args[0];
            rootFolder = @"C:\temp\mockprtest";

            MyUtility myUtility = new MyUtility(rootFolder, NEW_LICENSE_TEXT);

            while (true)
            {
                myUtility.Init();
                myUtility.Execute();

                Console.ReadLine();
            }
        }
    }

    internal class MyUtility
    {
        private string _rootFolder;
        private string _newLicenseText;
        private List<CodeFile> _codeFiles = new List<CodeFile>();

        public MyUtility(string rootFolder, string newLicenseText)
        {
            _rootFolder = rootFolder;
            _newLicenseText = newLicenseText;
        }

        internal void Init()
        {
            _codeFiles = new List<CodeFile>();
        }

        internal void Execute()
        {
            System.IO.DirectoryInfo rootFolder = new DirectoryInfo(_rootFolder);

            getAllCodeFilePaths(rootFolder);

            readAllCodeFilesToMemory();

            updateAllCodeFilesInMemory();

            writeAllCodeFilesToDisk();
        }

        void updateAllCodeFilesInMemory()
        {
            foreach (var codeFile in _codeFiles)
            {
                string[] codeFileLines = System.IO.File.ReadAllLines(codeFile.Path);

                codeFileLines[0] = string.Empty;
                codeFileLines[0] = _newLicenseText;
                codeFileLines[1] = ""; 

                codeFile.Text = string.Join("\n", codeFileLines);

                System.Threading.Thread.Sleep(1);
            }
        }

        private void readAllCodeFilesToMemory()
        {
            foreach (var codeFile in _codeFiles)
            {
                codeFile.Text =  
                    System.IO.File.ReadAllText(codeFile.Path);

                System.Threading.Thread.Sleep(1);
            }
        }

        private void writeAllCodeFilesToDisk()
        {
            foreach (var codeFile in _codeFiles)
            {
                System.IO.File.WriteAllText(codeFile.Path, codeFile.Text);

                System.Threading.Thread.Sleep(1);
            }


        }

        private void getAllCodeFilePaths(System.IO.DirectoryInfo rootFolder)
        {

            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subFolders = null;

            // First, process all the files directly under this folder
            try
            {
                files = rootFolder.GetFiles("*.*");
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception)
            {
            }

            if (files != null)
            {
                foreach (System.IO.FileInfo file in files)
                {
                    Console.WriteLine(file.FullName);
                    
                    var codeFile = new CodeFile(file.FullName);

                    _codeFiles.Add(codeFile);
                }

                // Now find all the subdirectories under this directory.
                subFolders = rootFolder.GetDirectories();

                foreach (System.IO.DirectoryInfo subFolder in subFolders)
                {
                    // method calls itself for each subdirectory.
                    getAllCodeFilePaths(subFolder);
                }
            }
        }

        public class CodeFile: ICodeFile
        {
            public string Path { get; set; }

            public string Text { get; set; }

            internal CodeFile(string path)
            {
                Path = path;
            }
        }

        interface ICodeFile
        {
            string Path { get; set; }

            string Text { get; set; }
        }
    }
   
    // // testing some stuff
    // var myString = "";
    // string myString = (string)string.Empty;
    //

    //
    //  Unit Tests
    //
    class MyUtilityTests
    {
        public void RunAll()
        {
            MyUtility_GracefullyFails_WhenRootFolderPassedNull();
            CodeFile_GracefullyFails_WhenPathPassedNull();
        }

        void MyUtility_GracefullyFails_WhenRootFolderPassedNull()
        {
            string rootFolder = null;
            string licenseText = null;

            MyUtility myUtility = new MyUtility(rootFolder, licenseText);

            if (myUtility != null)
            {
                Console.WriteLine("Test: Passed");
            }
            else
            {
                Console.WriteLine("Test: Failed");
            }
        }

        void CodeFile_GracefullyFails_WhenPathPassedNull()
        {
            string path = null;

            var codeFile = new MyUtility.CodeFile(path);

            if (codeFile != null)
            {
                Console.WriteLine("Test: Passed");
            }
            else
            {
                Console.WriteLine("Test: Failed");
            }
        }
    }
}
